using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Seleniumv2
{
    public class CoursesPage : PageBase {

        public IWebElement coursesCalendarTitle; 

        public CoursesPage(IWebDriver driver):base(driver){
        }

        public string getHeader() {
            
            coursesCalendarTitle = driver.FindElement(By.XPath("(//h2)[1]"));

            return coursesCalendarTitle.Text;
        }

    }
}