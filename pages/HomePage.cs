using NUnit.Framework;
using OpenQA.Selenium;
using SeleniumExtras.PageObjects;

namespace Seleniumv2
{
    public class HomePage : PageBase {

        public IWebElement loginButton;

        public HomePage(IWebDriver driver):base(driver){
        }

        public FederationPage clickLogin() {
            
            loginButton = driver.FindElement(By.ClassName("button"));

            loginButton.Click();

            return new FederationPage(driver);
        }

    }
}