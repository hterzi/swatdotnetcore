using System;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Seleniumv2
{
    public class FederationPage : PageBase {

        public IWebElement userNameField;
        public IWebElement nextButton;
        public IWebElement passwordField;
        public IWebElement continueButton;        
        public IWebElement noButton;

        public FederationPage(IWebDriver driver):base(driver){   
        }
        
        public FederationPage setUsername(string username) {

            userNameField = driver.FindElement(By.XPath("//input[contains(@name,'loginfmt')]"));

            // Send username to username field
            userNameField.SendKeys(username);
            
            return this;
        }

        public FederationPage setPassword(string password) {

            passwordField = driver.FindElement(By.Name("passwd"));

            // Send password to password field
            passwordField.SendKeys(password);
            
            return this;
        }

        public FederationPage clickNext() {

            nextButton = driver.FindElement(By.Id("idSIButton9"));

            // Click next button
            nextButton.Click();

            return this;
        }

        public FederationPage clickContinue() {

            continueButton = driver.FindElement(By.Id("idSIButton9"));
            
            // Click continue button
            continueButton.Click();

            return this;
        }

        public CoursesPage clickNo() {

            noButton = driver.FindElement(By.Id("idBtn_Back"));

            // Click no button
            noButton.Click();

            return new CoursesPage(driver);
        }

    }
}