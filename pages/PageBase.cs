using NUnit.Framework;
using OpenQA.Selenium;

namespace Seleniumv2
{
    public class PageBase {
        public IWebDriver driver;

        public PageBase(IWebDriver driver){
            this.driver = driver;
        }

    }
}