using NUnit.Framework;
using NUnit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace Seleniumv2
{
    [TestFixture]
    public class TestClass : TestBase {

    protected static CoursesPage coursesPage;

    [Test]
    public void VerifyPageHeader() {

        LoginExistingUser();

        coursesPage = new CoursesPage(driver);

        StringAssert.AreEqualIgnoringCase("Cursus kalender", coursesPage.getHeader());

        }
    }
}
