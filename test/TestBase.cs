using NUnit.Framework;
using NUnit;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace Seleniumv2
{
    [TestFixture]
    public class TestBase {

    protected static IWebDriver driver;
    protected static HomePage homePage;
    private static String application = "http://www.kzaconnected.nl/";

    [SetUp]

    public void SetupTest() {

        ChromeDriverService service = ChromeDriverService.CreateDefaultService(@System.IO.Directory.GetCurrentDirectory(), "chromedriver.exe");
        service.Port = 64444;
        driver = new ChromeDriver(service);
        driver.Manage().Window.Maximize();
        driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        driver.Url = application;
        homePage = new HomePage(driver);
    }

    [TearDown]
    public void TeardownTest() {

        driver.Quit();
    }

    public CoursesPage LoginExistingUser(){

        homePage
            .clickLogin()
            .setUsername("use your own username")
            .clickNext()
            .setPassword("use your own password")
            .clickContinue()
            .clickNo();

        return new CoursesPage(driver);
    }

    }
}
